<?php
// web/index.php
require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

$app = new Silex\Application();

// Convert plain text JSON format into JSON
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        if (json_last_error() === JSON_ERROR_NONE) {
            $request->request->replace($data);
        }
    }
});

// Controller
$app->mount('/api', new AppBundle\Controller\ApiController());

$app->run();