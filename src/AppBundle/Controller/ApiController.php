<?php
namespace AppBundle\Controller;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $factory = $app['controllers_factory'];
        $factory->get('/get','AppBundle\Controller\ApiController::getAction');
        $factory->post('/','AppBundle\Controller\ApiController::postAction');

        return $factory;
    }

    public function getAction(Application $app, Request $request)
    {
        return new JsonResponse(['status' => true,], 200, ['Content-Type' => 'application/json',]);
    }

    public function postAction(Application $app, Request $request)
    {
        $post = $request->request->all();

        return new JsonResponse(['status' => true,], 200, ['Content-Type' => 'application/json',]);
    }
}